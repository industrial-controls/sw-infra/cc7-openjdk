# CC7 OpenJDK

A Docker image build for CentOS7 and OpenJDK.

# How to release

First, start a gitflow release branch:

    mvn -B gitflow:release-start
    
Then, refine the release as needed. When you are ready :

    mvn -B gitflow:release-finish
    
The release will be tagged and automatically deploy by Gitlab CI.
If Gitlab CI does not work, you can also execute :

    mvn -B deploy gitflow:release-finish

